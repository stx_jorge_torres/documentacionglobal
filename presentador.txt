
PRESENTADOR

SPLASH
	- validarApertura()
		- IF NOT modelo.jwtGet()
			- vista.showLogin()
		- ELSE 
			- modeloWs.validarSesion(callback_validar_sesion)	

	- callback_validar_sesion(RESPUESTA)
		- 200 
			- empleado = modelo.infoEmpleadoGet()
			- IF empleado.PERFIL == "VENDEDOR"
				- vista.mostrarPrincipalVendedor()
			- ELSE
				- vista.mostrarPricipalSupervisor()
		- 401-01
			- modelo.cerrarSesion()
			- vista.mostrarLogin()
		- ELSE
			- vista.mostrarLogin()

LOGIN
	- ATRIBUTOS
		- NUMERO

	- iniciarVista()
		- adaptador = Adapter(modelo.plantasGet())
		- vista.iniciarSpinner(adaptador)

	- iniciarSesion(NUMERO, CONTRASENA, PLANTA)
		- IF NOT validar(NUMERO, TRUE)
			- vista.alert("El número de empleado es invalido")
			- RETURN
		- IF NOT validar(CONTRASENA, TRUE)
			- vista.alert("La contraseña es invalida")
			- RETURN
		- this.numero = NUMERO
		- modelo.instanciaSet(PLANTA.instancia)
		- vista.showProgressDialog()
		- modeloWs.obtenerApikey(NUMERO, CONTRASENA, callback_obtener_apikey)

	- callback_obtener_apikey(RESPUESTA)
		- IF RESPUESTA.code != 200
			- vista.hideProgressDialog()
		- 200
			- modelo.apikeySet(RESPONSE.apikey)
			- modeloWs.obtenerJwt(NUMERO, callback_obtener_jwt)
		- 400 
			- vista.alert("Ocurrió un error. Intenta de nuevo.")
		- 401-01
			- vista.alert("Ocurrió un error. Intenta de nuevo.")
		- 402-01
			- vista.alert("El empleado no existe")
		- 402-02
			- vista.alert("La contraseña es incorrecta")
		- else           
			- vista.alert("Ocurrió un error. Intenta de nuevo.")

	- callback_obtener_jwt(RESPUESTA)
		- IF RESPUESTA.code != 200
			- vista.hideProgressDialog()
		-200
			- modelo.jwtSet(RESPUESTA.jwt)
			- modeloWs.consultarEmpleado(callback_empleado_consultar)
		- 402-01
			- vista.alert("El empleado no existe")
		- 402-02
			- vista.alert("Ocurrió un error. Intente de nuevo.")
		- else
			- vista.alert("Ocurrió un error. Intente de nuevo.")

	- callback_empleado_consultar(RESPUESTA)
		- vista.hideProgressDialog()
		- 200
			- empleado = RESPUESTA.empleado
			- modelo.infoEmpleadoSet(empleado.NOMBRE, empleado.NUMERO, empleado.TELEFONO, empleado.IMAGEN, empleado.PERFIL, empleado.ID_GLOBALNET_UNIDAD, empleado.TIPO_UNIDAD)

			- G_EMPLEADO = modelo.infoEmpleadoGet()
			- IF G_EMPLEADO.PERFIL == "VENDEDOR"
				- vista.loginExitosoVendedor()
			- ELSE
				- vista.loginExitosoSupervisor()
		- 401-01
			- modelo.cerrarSesión()
			- vista.alert("Ocurrió un error. Intente de nuevo.")
		- ELSE
			- vista.alert("Ocurrió un error. Intente de nuevo.")

PEDIDOS

	- nuevoServicio()
		- G_INFO = modelo.infoEmpleadoGet()
		- IF G_INFO.TIPO_UNIDAD = "PORTATIL"
			- vista.nuevoServicio(CILINDRO)
		- ELSE
			- vista.nuevoServicio(ESTACIONARIO)

	- mostrarTodo()
		- G_LISTA = modeloSqlite.consultarServicios()
		- adaptador = Adaptador(G_LISTA)

	- mostrarPendientes()
		- G_LISTA = modeloSqlite.consultarServiciosPendientes()
		- adaptador = Adaptador(G_LISTA)

	- abrirServicio(INFO)
		- G_INFO = modelo.infoEmpleadoGet()
		- IF G_INFO.TIPO_UNIDAD = "PORTATIL"
			- pantalla = CILINDRO()
			- pantalla.INFO = INFO
			- vista.abrirServicio(pantalla)
		- ELSE
			- pantalla = ESTACIONARIO()
			- pantalla.INFO = INFO
			- vista.abrirServicio(pantalla)

	- servicioCancelado(INFO_ELEMENTO)
		- modeloSqlite.servicioCancelar(INFO_ELEMENTO.ID_GLOBALNET, INFO_ELEMENTO.RAZON_CANCELACION)
		- G_DATOS_CANCELACION : {
			TIPO : "SERVICIOS.CANCELAR",
			ID_GLOBALNET: INFO_ELEMENTO.ID_GLOBALNET,
			FECHA_HORA: NOW(),
			RAZON_CANCELACION : INFO_ELEMENTO.RAZON_CANCELACION
		}
		- modeloSqlite.sgregarDatos(G_DATOS_CANCELACION)

	- servicioEnProceso(INFO_ELEMENTO)
		- modeloSqlite.servicioEnProceso(INFO_ELEMENTO.ID_GLOBALNET)
		- G_DATOS_EN_PROCESO : {
			TIPO: "SERVICIOS.EN_PROCESO",
			ID_GLOBALNET : INFO_ELEMENTO.ID_GLOBALNET,
			FECHA_HORA : NOW()
		}
		- modeloSqlite.agregarDatos(G_DATOS_EN_PROCESO)		

PRINCIPAL
	- SIDE_MENU
		- cerrarSesion()
			- modelo.cerrarSesion()
			- vista.sesionCerrada()

		- sincronizar()
			- vista.showProgressDialog()
			- modeloWs.cargarDatos(callbak_sincronizar_subida)

		- callbak_sincronizar_subida(RESPUESTA)
			- IF RESPUESTA.code != 200
				- vista.hideProgressDialog()
			- 200
				- modeloSqlite.BorrarDatos()
				- modeloWs.descargarDatos(callback_sincronizar_bajada)
			- 400
				- vista.alert("Ocurrió un error. Intente de nuevo.")
			- 401-01
				- ???
			- else
				- vista.alert("Ocurrió un error. Intente de nuevo.")

		- callback_sincronizar_bajada(RESPUESTA)
			- IF RESPUESTA.code != 200
				- vista.hideProgressDialog()
			- 200
				- FOREACH obj IN RESPUESTA
					- SWITCH(obj.TIPO)
						- "PRECIOS.AGREGAR" 
							- modeloSqlite.AgregarPrecio(obj.ID_GLOBALNET, obj.FECHA_ULTIMA_MODIFICACION, obj.INDICE, obj.PRODUCTO, obj.ANIO, obj.MES, obj.PRECIO_NETO)

						- "PRECIOS.ACTUALIZAR"
							- modeloSqlite.ActualizarPrecio(obj.ID_GLOBALNET, obj.FECHA_ULTIMA_MODIFICACION, obj.INDICE, obj.PRODUCTO, obj.ANIO, obj.MES, obj.PRECIO_NETO)

						- "SERVICIOS.AGREGAR"
							- modeloSqlite.AgregarServicio(obj.ID_GLOBALNET, obj.FECHA_ULTIMA_MODIFICACION, obj.CLIENTE, obj.CUENTA, obj.CUENTA_OCULTA, obj.ID_GLOBALNET_UNIDAD, obj.TIPO_SERVICIO, obj.FECHA_PROGRAMADO, obj.ESTATUS, obj.RAZON_CANCELACION, obj.DOMICILIO, obj.INDICE, obj.DESCUENTO, obj.APLICA_CREDITO)

						- "SERVICIOS.ACTUALIZAR"
							- modeloSqlite.ActualizarServicio(obj.ID_GLOBALNET, obj.FECHA_ULTIMA_MODIFICACION, obj.CLIENTE, obj.CUENTA, obj.CUENTA_OCULTA, obj.ID_GLOBALNET_UNIDAD, obj.TIPO_SERVICIO, obj.FECHA_PROGRAMADO, obj.ESTATUS, obj.RAZON_CANCELACION, obj.DOMICILIO, obj.INDICE, obj.DESCUENTO, obj.APLICA_CREDITO)

						- "SERVICIOS.EN_PROCESO"
							- modeloSqlite.ServicioEnProceso(obj.ID_GLOBALNET)

						- "CUENTAS_OCULTAS.AGREGAR"
							- modeloSqlite.AgregarCuentaOculta(obj.ID_GLOBALNET, obj.CUENTA_OCULTA, obj.FECHAHORA, obj.ID_GLOBALNET_UNIDAD, obj.ESTATUS)

						- "CUENTAS_OCULTAS.OCUPAR"
							- modeloSqlite.UsarCuentaOculta(obj.ID_GLOBALNET)
					- modelo.UltimoIdSet(obj.ID)
				- modeloWs.consultarEmpleado(callback_sincronizar_usuario)
			- 401-01
				- ??? 
			- else
				- vista.alert("Ocurrió un error. Intente de nuevo.")

		- callback_sincronizar_usuario(RESPUESTA)
			- vista.hideProgressDialog()
			- 200
				- empleado = RESPUESTA.empleado
				- modelo.infoEmpleadoSet(empleado.NOMBRE, empleado.NUMERO, empleado.TELEFONO, empleado.IMAGEN, empleado.PERFIL, empleado.ID_GLOBALNET_UNIDAD, empleado.TIPO_UNIDAD)
				- vista.sincronizado()
			- 401-01
				- ??? 
			- ELSE
				- vista.alert("Ocurrió un error. Intente de nuevo.")

LIQUIDACION
	- cargarDatos()
		- G_EFECTIVO = modeloSqlite.obtenerEfectivo()
		- G_TDC = modeloSqlite.obtenerTDC()
		- G_TDD = modeloSqlite.obtenerTDD()
		- G_TRANSFERENCIA = modeloSqlite.obtenerTransferencias()
		- G_CHEQUE = modeloSqlite.obtenerCheque()
		- G_TOTAL = modeloSqlite.obtenerTotal()
		- G_CREDITO = modeloSqlite.obtenerCredito()
		- DATOS : {
			EFECTIVO : {
				TOTAL : G_EFECTIVO.TOTAL
				NUMERO_VENTAS : G_EFECTIVO.VENTAS
			},
			TDC : {
				TOTAL : G_TDC.TOTAL
				NUMERO_VENTAS : G_TDC.VENTAS
			},
			TDD : {
				TOTAL : G_TDD.TOTAL
				NUMERO_VENTAS : G_TDD.VENTAS
			},
			TRANSFERENCIA : {
				TOTAL : G_TRANSFERENCIA.TOTAL
				NUMERO_VENTAS : G_TRANSFERENCIA.VENTAS
			},
			CHEQUE : {
				TOTAL : G_CHEQUE.TOTAL
				NUMERO_VENTAS : G_CHEQUE.VENTAS
			},
			TOTAL : {
				TOTAL : G_TOTAL.TOTAL
				NUMERO_VENTAS : G_TOTAL.VENTAS
			},
			CREDITO : {
				TOTAL : G_CREDITO.TOTAL
				NUMERO_VENTAS : G_CREDITO.VENTAS
			}
		}
		- vista.mostrarDatos(DATOS)

PEDIDOS.CILINDRO
	- ATRIBUTOS
		- REFERENCIA : {
			"CANTIDAD_10" : 0,
			"CANTIDAD_20" : 0,
  			"CANTIDAD_30" : 0,
  			"CANTIDAD_45" : 0,
  			"CANTIDAD_KILOS" : 0,
  			"PRECIO_10" : 0, //COMO INSTANCIAR EL PRECIO??
  			"PRECIO_20" : 0,
  			"PRECIO_30" : 0,
  			"PRECIO_45" : 0,
  			"PRECIO_KILOS" : 0
  		}

  		- INFO
  		- CANTIDAD_TOTAL
  		- PAGO_TOTAL
  		- TIPO_PAGO

	- verificarServicio (INFO_SERVICIO)
		- INFO = INFO_SERVICIO
		- APLICA_CREDITO = FALSE

		- IF INFO
			- APLICA_CREDITO = INFO.APLICA_CREDITO
			
			- REFERENCIA.PRECIO_10 = modeloSqlite.consultarPrecio(10, INFO.INDICE)
			- REFERENCIA.PRECIO_20 = modeloSqlite.consultarPrecio(20, INFO.INDICE)
			- REFERENCIA.PRECIO_30 = modeloSqlite.consultarPrecio(30, INFO.INDICE)
			- REFERENCIA.PRECIO_45 = modeloSqlite.consultarPrecio(40, INFO.INDICE)
			- REFERENCIA.PRECIO_KILOS = modeloSqlite.consultarPrecio(50, INFO.INDICE)
			
			- NOMBRE = INFO.DIRECCION.NOMBRE
			- DIRECCION = INFO.DIRECCION.CALLE + INFO.DIRECCION.NUMERO + INFO.DIRECCION.COLONIA + INFO.DIRECCION.CP + INFO.DIRECCION.CIUDAD + INFO.DIRECCION.ESTADO + INFO.DIRECCION.PAIS

			- vista.configurarDatoscuenta(NOMBRE, DIRECCION)
		- ELSE
			- REFERENCIA.PRECIO_10 = modeloSqlite.consultarPrecio(10, 0)
			- REFERENCIA.PRECIO_20 = modeloSqlite.consultarPrecio(20, 0)
			- REFERENCIA.PRECIO_30 = modeloSqlite.consultarPrecio(30, 0)
			- REFERENCIA.PRECIO_45 = modeloSqlite.consultarPrecio(40, 0)
			- REFERENCIA.PRECIO_KILOS = modeloSqlite.consultarPrecio(50, 0)

			- NOMBRE = "Público General"
			- vista.configurarDatosCuentaNueva(NOMBRE)

  		- adapter = Adapter(modelo.tiposPagoGet(APLICA_CREDITO))
  		- initSpinnerTiposPago(adapter)

	- modificarCilindro10(OPERACION["AGREGAR" | "QUITAR"])
		- IF OPERACION == "AGREGAR"
			- REFERENCIA.CANTIDAD_10 = REFERENCIA.CANTIDAD_10 + 1
		- ELSE
			- REFERENCIA.CANTIDAD_10 = REFERENCIA.CANTIDAD_10 - 1
		- CANTIDAD = REFERENCIA.CANTIDAD_10
		- TOTAL = REFERENCIA.CANTIDAD_10 * REFERENCIA.PRECIO_10
		- this.actualizarTotales()
		- vista.modificarCantidadCilindro10(CANTIDAD, CANTIDAD_TOTAL, TOTAL, PAGO_TOTAL)

	- modificarCilindro20(OPERACION["AGREGAR" | "QUITAR"])
		- IF OPERACION == "AGREGAR"
			- REFERENCIA.CANTIDAD_20 = REFERENCIA.CANTIDAD_20 + 1
		- ELSE
			- REFERENCIA.CANTIDAD_20 = REFERENCIA.CANTIDAD_20 - 1
		- CANTIDAD = REFERENCIA.CANTIDAD_20
		- TOTAL = REFERENCIA.CANTIDAD_20 * REFERENCIA.PRECIO_20
		- this.actualizarTotales()
		- vista.modificarCantidadCilindro20(CANTIDAD, CANTIDAD_TOTAL, TOTAL, PAGO_TOTAL)

	- modificarCilindro30(OPERACION["AGREGAR" | "QUITAR"])
		- IF OPERACION == "AGREGAR"
			- REFERENCIA.CANTIDAD_30 = REFERENCIA.CANTIDAD_30 + 1
		- ELSE
			- REFERENCIA.CANTIDAD_30 = REFERENCIA.CANTIDAD_30 - 1
		- CANTIDAD = REFERENCIA.CANTIDAD_30
		- TOTAL = REFERENCIA.CANTIDAD_30 * REFERENCIA.PRECIO_30
		- this.actualizarTotales()
		- vista.modificarCantidadCilindro30(CANTIDAD, CANTIDAD_TOTAL, TOTAL, PAGO_TOTAL)

	- modificarCilindro45(OPERACION["AGREGAR" | "QUITAR"])
		- IF OPERACION == "AGREGAR"
			- REFERENCIA.CANTIDAD_45 = REFERENCIA.CANTIDAD_45 + 1
		- ELSE
			- REFERENCIA.CANTIDAD_45 = REFERENCIA.CANTIDAD_45 - 1
		- CANTIDAD = REFERENCIA.CANTIDAD_45
		- TOTAL = REFERENCIA.CANTIDAD_45 * REFERENCIA.PRECIO_45
		- this.actualizarTotales()
		- vista.modificarCantidadCilindro45 (CANTIDAD, CANTIDAD_TOTAL, TOTAL, PAGO_TOTAL)

	- modificarCilindroKilos(CANTIDAD)
		- REFERENCIA.CANTIDAD_KILOS = CANTIDAD
		- CANTIDAD = REFERENCIA.CANTIDAD_KILOS
		- TOTAL = REFERENCIA.CANTIDAD_KILOS * REFERENCIA.PRECIO_KILOS
		- this.actualizarTotales()
		- vista.modificarCantidadCilindroKilos(CANTIDAD, CANTIDAD_TOTAL, TOTAL, PAGO_TOTAL)

	- actualizarTotales()
		- CANTIDAD_TOTAL = REFERENCIA.CANTIDAD_10 + REFERENCIA.CANTIDAD_20 + REFERENCIA.CANTIDAD_30 + REFERENCIA.CANTIDAD_45 + REFERENCIA.CANTIDAD_KILOS
		- PAGO_TOTAL = (REFERENCIA.CANTIDAD_10 * REFERENCIA.PRECIO_10) + (REFERENCIA.CANTIDAD_20 * REFERENCIA.PRECIO_20) + (REFERENCIA.CANTIDAD_30 * REFERENCIA.PRECIO_30) + (REFERENCIA.CANTIDAD_45 * REFERENCIA.PRECIO_45) + (REFERENCIA.CANTIDAD_KILOS * REFERENCIA.PRECIO_KILOS)

	- imprimirTicket()
		- ????? AUN NO SE COMO SE HACE

	- guardarServicio(METODO_PAGO)
		- TIPO_PAGO = METODO_PAGO
		- modelo.LocalizacionGet(callback_localizacion)

	- callback_localizacion(RESPUESTA)
		- modeloSqlite.ServicioSurtido(INFO.ID_GLOBALNET)
		- FOLIO = InfoEmpleadoGet()["ID_UNIDAD"] + RANDOM(4 DIGITOS)
		- G_DATO_NOTA = {
			TIPO : "NOTAS.AGREGAR",
			ID_GLOBALNET_UNIDAD : modelo.infoEmpleadoGet()["ID_GLOBALNET_UNIDAD"],
			FECHA_HORA : NOW(),
			CLIENTE : INFO.CLIENTE || 0,
			CUENTA : INFO.CUENTA || 0,
			FOLIO : FOLIO,
			MONTO : PAGO_TOTAL,
			TIPO_PAGO : TIPO_PAGO,
			DESCUENTO : INFO.DESCUENTO || 0,
			LATITUD : RESPONSE.LATITUD,
			LONGITUD : RESPONSE.LONGITUD,
			REFERENCIA : REFERENCIA
			ID_GLOBALNET_SERVICIO : INFO.ID_GLOBALNET_SERVICIO || 0
		}
		- modeloSqlite.AgregarNota(G_DATO_NOTA.CLIENTE, G_DATO_NOTA.CUENTA, G_DATO_NOTA.FOLIO, G_DATO_NOTA.MONTO, G_DATO_NOTA.TIPO_PAGO, G_DATO_NOTA.DESCUENTO, G_DATO_NOTA.LATITUD, G_DATO_NOTA.LONGITUD, G_DATO_NOTA.REFERENCIA, G_DATO_NOTA.ID_GLOBALNET_SERVICIO)
		- modeloSqlite.AgregarDatos(G_DATO_NOTA)
		- vista.ventaExitosa()

PEDIDOS.ESTACIONARIO

	- ATRIBUTOS
		- REFERENCIA : {
			"LITROS" : 0,
			"PRECIO" : 0
  		}
	
  		- TIPO_PAGO
  		- CUENTA_OCULTA

	- verificarServicio(INFO_SERVICIO)
		- INFO = INFO_SERVICIO
		- IF INFO
			- REFERENCIA.PRECIO = modeloSqlite.ConsultarPrecio(60, INFO.INDICE)
			- NOMBRE = INFO.DIRECCION.NOMBRE
			- DIRECCION = INFO.DIRECCION.CALLE + INFO.DIRECCION.NUMERO + INFO.DIRECCION.COLONIA + INFO.DIRECCION.CP + INFO.DIRECCION.CIUDAD + INFO.DIRECCION.ESTADO + INFO.DIRECCION.PAIS
			- CUENTA_OCULTA = INFO.CUENTA_OCULTA
			- vista.configurarDatosCuenta(NOMBRE, DIRECCION, CUENTA_OCULTA)
		- ELSE
			- REFERENCIA.PRECIO = modeloSqlite.ConsultarPrecio(60, 0)
			- NOMBRE = "Público General"
			- CUENTA_OCULTA = modeloSqlite.ObtenerCuentaOculta()
			- vista.configurarDatosCuentaNueva(NOMBRE, CUENTA_OCULTA)

		- adapter = Adapter(modelo.tiposPagoGet(INFO.APLICA_CREDITO))
  		- initSpinnerTiposPago(adapter)

	- modificarLitros(CANTIDA_LITROS)
		- REFERENCIA.LITROS = CANTIDA_LITROS
		- TOTAL = CANTIDAD_LITROS * REFERENCIA.PRECIO
		- vista.modificarCantidadLitros (CANTIDA_LITROS, LITROS)

	- imprimirTicket()
		- ????? AUN NO SE COMO SE HACE

	- guardarServicio(METODO_PAGO)
		- TIPO_PAGO = METODO_PAGO
		- modelo.LocalizacionGet(callback_localizacion)

	- callback_localizacion(RESPUESTA)
		- modeloSqlite.servicioSurtido(INFO.ID_GLOBALNET)
		- FOLIO = modelo.infoEmpleadoGet()["ID_UNIDAD"] + RANDOM(4 DIGITOS)
		- G_DATO_NOTA = {
			TIPO : "NOTAS.AGREGAR",
			ID_GLOBALNET_UNIDAD : ,
			FECHA_HORA : NOW(),
			CLIENTE : INFO.CLIENTE || 0,
			CUENTA : INFO.CUENTA || 0,
			FOLIO : FOLIO,
			MONTO : PAGO_TOTAL,
			TIPO_PAGO : TIPO_PAGO,
			DESCUENTO : INFO.DESCUENTO || 0,
			LATITUD : RESPONSE.LATITUD,
			LONGITUD : RESPONSE.LONGITUD,
			REFERENCIA : REFERENCIA
			ID_GLOBALNET_CUENTA_OCULTA : INFO.ID_GLOBALNET_CUENTA_OCULTA || CUENTA_OCULTA
		}
		- modeloSqlite.agregarNota(G_DATO_NOTA.CLIENTE, G_DATO_NOTA.CUENTA, G_DATO_NOTA.FOLIO, .G_DATO_NOTA.MONTO, G_DATO_NOTA.TIPO_PAGO, G_DATO_NOTA.DESCUENTO, G_DATO_NOTA.LATITUD, G_DATO_NOTA.LONGITUD, G_DATO_NOTA.REFERENCIA, G_DATO_NOTA.ID_GLOBALNET_CUENTA_OCULTA)
		- modeloSqlite.agregarDatos(G_DATO_NOTA)
		- vista.ventaExitosa()

SUPERVISOR
	
	- ATRIBUTOS
		- TIPO_UNIDAD

	- iniciarVista()
		- vista.showProgressDialog()
		- modeloWs.consultarUnidades(callback_consultar_unidades)

	- callback_consultar_unidades(RESPUESTA)
		- vista.hideProgressDialog()
		- 200
			- adaptador = Adaptador(RESPUESTA.unidades)
			- vista.iniciarSpinnerUnidades(adaptador)
		- 401-01
			- ???
		- else
			- vista.alert("Ocurrió un error. Intente de nuevo.")

	- abrirLiquidacion(UNIDAD)
		- TIPO_UNIDAD = UNIDAD.TIPO_UNIDAD
		- vista.showProgressDialog()
		- modeloWs.consultarLiquidaciones(UNIDAD.ID_GLOBALNET, callback_consultar_liquidacion)

	- callback_consultar_liquidacion(RESPUESTA)
		- vista.hideProgressDialog()
		- 200
			- IF TIPO_UNIDAD == "ESTACIONARIO"
				- vista.hideBotonIniciarViajeCilindro()
				- vista.hideBotonFinalizarViajeCilindro()
				- vista.hideLiquidacionCilindroInicial()
				- vista.hideLiquidacionCilindroFinal()
				- vista.showLiquidacionEstacionarioInicial()
				- vista.cargarDatosLiquidacionEstacionarioInicial(RESPUESTA.liquidacion.SALIO_CON)
				
				- IF EXIST(RESPUESTA.liquidacion.REGRESO_CON)
					- vista.showLiquidacionEstacionarioFinal()
					- vista.showBotonFinalizarViajeEstacionario()
					- vista.hideBotonIniciarViajeEstacionario()
					- vista.cargarDatosLiquidacionEstacionarioFinal(RESPUESTA.liquidacion.REGRESO_CON)
				- ELSE
					- vista.showBotonIniciarViajeEstacionario()
					- vista.hideBotonFinalizarViajeEstacionario()
			- ELSE
				- vista.hideBotonIniciarViajeEstacionario()
				- vista.hideBotonFinalizarViajeEstacionario()
				- vista.hideLiquidacionEstacionarioInicial()
				- vista.hideLiquidacionEstacionarioFinal()
				- vista.showLiquidacionCilindroInicial()
				- vista.cargarDatosLiquidacionCilindroInicial(RESPUESTA.liquidacion.SALIO_CON)
				
				- IF EXIST(RESPUESTA.liquidacion.REGRESO_CON)
					- vista.showLiquidacionCilindroFinal()
					- vista.showBotonFinalizarViajeCilindro()
					- vista.hideBotonIniciarViajeCilindro()
					- vista.cargarDatosLiquidacionCilindroFinal(RESPUESTA.liquidacion.REGRESO_CON)
				- ELSE
					- vista.showBotonIniciarViajeCilindro()
					- vista.hideBotonFinalizarViajeCilindro()

		- 401-01
			- ???

		- else
			- vista.alert("Ocurrio un error. Intent de nuevo.")

	- iniciarViajeEstacionario(MEDIDOR, ROTOGAUGE, UNIDAD)
		- vista.showProgressDialog()
		- modeloWs.iniciarViajeEstacionario(UNIDAD.ID_GLOBALNET, MEDIDOR, ROTOGAUGE, callback_iniciar_viaje_estacionario)

	- callback_iniciar_viaje_estacionario(RESPUESTA)
		- vista.hideProgressDialog()
		- 200 
			- vista.alert("Se ha iniciado el viaje correctamente")
			- this.resetVista()
		- 401-01
			- ???
		- 402-01
			- vista.alert("Ocurrió un error. Intente de nuevo.") **NO ES SUPERVISOR**
		- 402-02
			- vista.alert("Ocurrió un error. Intente de nuevo.") **UNIDAD NO EXISTE**
		- 402-03
			- vista.alert("Ocurrió un error. Intente de nuevo.") **FECHA INVALIDA**
		- else
			- vista.alert("Ocurrió un error. Intente de nuevo.")

	- finalizarViajeEstacionario(MEDIDOR, ROTOGAUGE, UNIDAD)
		- vista.showProgressDialog()
		- modeloWs.finalizarViajeEstacionario(UNIDAD.ID_GLOBALNET, MEDIDOR, ROTOGAUGE, callback_finalizar_viaje_estacionario)

	- callback_finalizar_viaje_estacionario(RESPUESTA)
		- vista.hideProgressDialog()
		- 200 
			- vista.alert("Se ha finalizado el viaje correctamente")
			- this.resetVista()
		- 401-01
			- ???
		- 402-01
			- vista.alert("Ocurrió un error. Intente de nuevo.") **NO ES SUPERVISOR**
		- 402-02
			- vista.alert("Ocurrió un error. Intente de nuevo.") **UNIDAD NO EXISTE**
		- 402-03
			- vista.alert("Ocurrió un error. Intente de nuevo.") **FECHA INVALIDA**
		- else
			- vista.alert("Ocurrió un error. Intente de nuevo.")

	- iniciarViajeCilindro(UNIDAD)
		- vista.showProgressDialog()
		- modeloWs.iniciarViajeCilindro(UNIDAD.ID_GLOBALNET, callback_iniciar_viaje_cilindro)

	- callback_iniciar_viaje_cilindro(RESPUESTA)
		- vista.hideProgressDialog()
		- 200 
			- vista.alert("Se ha finalizado el viaje correctamente")
			- this.resetVista()
		- 401-01
			- ???
		- 402-01
			- vista.alert("Ocurrió un error. Intente de nuevo.") **NO ES SUPERVISOR**
		- 402-02
			- vista.alert("Ocurrió un error. Intente de nuevo.") **UNIDAD NO EXISTE**
		- 402-03
			- vista.alert("Ocurrió un error. Intente de nuevo.") **FECHA INVALIDA**
		- else
			- vista.alert("Ocurrió un error. Intente de nuevo.")

	- finalizarViajeCilindro(UNIDAD)
		- vista.showProgressDialog()
		- modeloWs.finalizarViajeCilindro(UNIDAD.ID_GLOBALNET, callback_finalizar_viaje_cilindro)

	- callback_finalizar_viaje_cilindro(RESPUESTA)
		- vista.hideProgressDialog()
		- 200 
			- vista.alert("Se ha finalizado el viaje correctamente")
			- this.resetVista()
		- 401-01
			- ???
		- 402-01
			- vista.alert("Ocurrió un error. Intente de nuevo.") **NO ES SUPERVISOR**
		- 402-02
			- vista.alert("Ocurrió un error. Intente de nuevo.") **UNIDAD NO EXISTE**
		- 402-03
			- vista.alert("Ocurrió un error. Intente de nuevo.") **FECHA INVALIDA**
		- else
			- vista.alert("Ocurrió un error. Intente de nuevo.")

	- resetVista()
		- vista.seleccionDefaultSpinner()
		- vista.hideBotonIniciarViajeCilindro()
		- vista.hideBotonFinalizarViajeCilindro()
		- vista.hideLiquidacionCilindroInicial()
		- vista.hideLiquidacionCilindroFinal()
		- vista.hideLiquidacionEstacionarioInicial()
		- vista.hideLiquidacionEstacionarioFinal()
		- vista.hideBotonIniciarViajeEstacionario()
		- vista.hideBotonFinalizarViajeEstacionario()

OTROS EVENTOS
	- CADA X MINUTOS
		- sincronizar()
			- modeloWs.cargarDatos(callbak_sincronizar_subida)
			- modeloWs.descargarDatos(callback_sincronizar_bajada)
			- modeloWs.consultarEmpleado(callback_sincronizar_usuario)

		- callbak_sincronizar_subida(RESPUESTA)
			- 200
				- modeloSqlite.BorrarDatos()
			- ELSE
				- NO HACE NADA

		- callback_sincronizar_bajada(RESPUESTA)
			- 200
				- FOREACH obj IN RESPONSE
					- SWITCH(obj.TIPO)
						- "PRECIOS.AGREGAR" 
							- modeloSqlite.AgregarPrecio(obj.ID_GLOBALNET, obj.FECHA_ULTIMA_MODIFICACION, obj.INDICE, obj.PRODUCTO, obj.ANIO, obj.MES, obj.PRECIO_NETO)

						- "PRECIOS.ACTUALIZAR"
							- modeloSqlite.ActualizarPrecio(obj.ID_GLOBALNET, obj.FECHA_ULTIMA_MODIFICACION, obj.INDICE, obj.PRODUCTO, obj.ANIO, obj.MES, obj.PRECIO_NETO)

						- "SERVICIOS.AGREGAR"
							- modeloSqlite.AgregarServicio(obj.ID_GLOBALNET, obj.FECHA_ULTIMA_MODIFICACION, obj.CLIENTE, obj.CUENTA, obj.CUENTA_OCULTA, obj.ID_GLOBALNET_UNIDAD, obj.TIPO_SERVICIO, obj.FECHA_PROGRAMADO, obj.ESTATUS, obj.DOMICILIO, obj.INDICE, obj.DESCUENTO, obj.APLICA_CREDITO)

						- "SERVICIOS.ACTUALIZAR"
							- modeloSqlite.ActualizarServicio(obj.ID_GLOBALNET, obj.FECHA_ULTIMA_MODIFICACION, obj.CLIENTE, obj.CUENTA, obj.CUENTA_OCULTA, obj.ID_GLOBALNET_UNIDAD, obj.TIPO_SERVICIO, obj.FECHA_PROGRAMADO, obj.ESTATUS, obj.DOMICILIO, obj.INDICE, obj.DESCUENTO, obj.APLICA_CREDITO)

						- "SERVICIOS.EN_PROCESO"
							- modeloSqlite.ServicioEnProceso(obj.ID_GLOBALNET)

						- "CUENTAS_OCULTA.AGREGAR"
							- modeloSqlite.AgregarCuentaOculta(obj.ID_GLOBALNET, obj.CUENTA_OCULTA, obj.FECHAHORA, obj.ID_GLOBALNET_UNIDAD, obj.ESTATUS)

						- "CUENTAS_OCULTAS.OCUPAR"
							- modeloSqlite.UsarCuentaOculta(obj.ID_GLOBALNET)

					- modelo.UltimoIdSet(obj.ID)

			- ELSE
				- NO HACE NADA

		- callback_sincronizar_usuario(RESPUESTA)
			- 200
				- empleado = RESPUESTA.empleado
				- modelo.infoEmpleadoSet(empleado.NOMBRE, empleado.NUMERO, empleado.TELEFONO, empleado.IMAGEN, empleado.PERFIL, empleado.ID_GLOBALNET_UNIDAD, empleado.TIPO_UNIDAD)
			- ELSE
				- NO HACE NADA

